package CercDeRazaMinima
import "fmt"

func main(){
	var mat[100][100] int
	var row,col int
	fmt.Print("Linii: ")
	fmt.Scanln(&row)
	fmt.Print("Coloane: ")
	fmt.Scanln(&col)

	fmt.Println()
	fmt.Println("Matrice: ")
	fmt.Println()
	for i := 0; i < row; i++ {
		for j := 0; j < col; j++ {
			fmt.Printf("Elementele matricei %d %d :",i+1,j+1)
			fmt.Scanln(&mat[i][j])

		}
	}
	for i := 0; i < row; i++ {
		for j := 0; j < col; j++ {
			fmt.Printf(" %d ",mat[i][j])
			if(j==col-1){
				fmt.Println("")
			}
		}
	}

	var max[100] int
	var min=999999
	max[0]=0
	var poz int=0

	for i:=0;i<row;i++ {
		for j:=0;j<col;j++ {
			if max[poz]<mat[i][j] {
				max[poz]=mat[i][j]
			}
		}
		poz++
	}
	for i:=0;i<row;i++{
		if min>max[i]{
			min=max[i]
		}
	}
	fmt.Printf("Minimul dintre maximele de pe linii este: %d ",min)
}
