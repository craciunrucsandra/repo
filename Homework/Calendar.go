package main

import "fmt"
import "time"

func main() {

	currentDate := time.currentDate()
    
	myDate := time.Date(
		2019, 11, 11, 09, 12, 34, 234543345, time.UTC)
	fmt.Println("Choosen date: ", myDate)

	diff := currentDate.Sub(myDate)
	days := int16 (diff.Hours()/24)
	fmt.Println("Difference between 2 dates in days: ", days)

	year, month, day, hour, min, sec := diff(myDate, currentDate)
	fmt.Printf("diff %d years, %d months, %d days, %d hours, %d mins and %d seconds.", year, month, day, hour, min, sec)
	fmt.Println(" ")

	fmt.Println("Day of week: " ,myDate.Weekday())

	daysToAdd := 10
	daysToSubstract := 5
	fmt.Println("The sum between a date and a number of days: ", myDate.AddDate(0, 0, daysToAdd))
	fmt.Println("The difference between a date and a number of days: ", myDate.AddDate(0, 0, (-daysToSubstract)))

}

func diff(a, b time.Time) (year, month, day, hour, min, sec int) {
	if a.Location() != b.Location() {
		b = b.In(a.Location())
	}
	if a.After(b) {
		a, b = b, a
	}
	year1, month1, day1 := a.Date()
	year2, month2, day2 := b.Date()

	hour1, min1, sec1 := a.Clock()
	hour2, min2, sec2 := b.Clock()

	year = int(year2 - year1)
	month = int(month2 - month1)
	day = int(day2 - day1)
	hour = int(hour2 - hour1)
	min = int(min2 - min1)
	sec = int(sec2 - sec1)

	if sec < 0 {
		sec += 60
		min--
	}
	if min < 0 {
		min += 60
		hour--
	}
	if hour < 0 {
		hour += 24
		day--
	}
	if day < 0 {
		t := time.Date(year1, month1, 32, 0, 0, 0, 0, time.UTC)
		day += 32 - t.Day()
		month--
	}
	if month < 0 {
		month += 12
		year--
	}

	return
}